Create table Ruletas(
	Id_Ruleta int identity (1,1) primary key ,
	Estado bit default 0 ,
	Created_By varchar(50) default suser_Sname(),
	Created_On datetime default getdate()
)
go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 06-10-2021 
Description: procedimiento encargado de crear Ruletas
*/
-- =============================================

Create Procedure  Wp_Ins_Ruleta
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/
insert into Ruletas(Created_By)
select 'Prueba'

select 'Se creo la Ruleta con ID :'+ convert(varchar,@@IDENTITY)

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
grant exec on Wp_Ins_Ruleta   to Prueba
go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 06-10-2021 
Description: procedimiento encargado de traer las Ruletas creadas
*/
-- =============================================

Create Procedure  Wp_Get_Ruletas
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/
select 
	Id_Ruleta,
	Estado,
	Created_By,
	convert(varchar,Created_On,103) as Fecha
from Ruletas

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
grant exec on Wp_Get_Ruletas  to Prueba
go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 06-10-2021 
Description: procedimiento encargado de traer las Ruletas creadas
*/
-- =============================================

Create Procedure  Wp_Upd_Ruletas
	@Id_Ruleta int
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/
update Ruletas
set Estado = 1- CONVERT(int ,Estado)
where Id_Ruleta = @Id_Ruleta

Select case when Estado =1 then 'Se ha abierto la ruleta' else 'Se ha cerrado la ruleta' end
from Ruletas
where Id_Ruleta = @Id_Ruleta

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
grant exec on Wp_Upd_Ruletas   to Prueba
go
Create table Apuestas(
	Id_Apuesta int identity(1,1) primary key,
	Apuesta varchar(10),
	Premio float ,
	Color char(1),
	Created_By varchar(50) default suser_Sname(),
	Created_On datetime default getdate()
)
go
insert into Apuestas(Apuesta,Premio,Color)
select '1','5','N'
union select '2','5','R'
union select '3','5','N'
union select '4','5','R'
union select '5','5','N'
union select '6','5','R'
union select '7','5','N'
union select '8','5','R'
union select '9','5','N'
union select '10','5','R'
union select '11','5','N'
union select '12','5','R'
union select '13','5','N'
union select '14','5','R'
union select '15','5','N'
union select '16','5','R'
union select '17','5','N'
union select '18','5','R'
union select '19','5','N'
union select '20','5','R'
union select '21','5','N'
union select '22','5','R'
union select '23','5','N'
union select '24','5','R'
union select '25','5','N'
union select '26','5','R'
union select '27','5','N'
union select '28','5','R'
union select '29','5','N'
union select '30','5','R'
union select '31','5','N'
union select '32','5','R'
union select '33','5','N'
union select '34','5','R'
union select '35','5','N'
union select '36','5','R'
union select 'Negro','1.8','N'
union select 'Rojo','1.8','R'


go
-- =============================================
/*
Author:	Santiago Reyes 
Create date: 07-10-2021 
Description: procedimiento encargado de traer las posibles apuestas
*/
-- =============================================

create Procedure  Wp_Get_Apuestas
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

/*proceso a ejecutar*/
select 
	Id_Apuesta,
	Apuesta,
	Premio,
	Color,
	Created_By,
	convert(varchar,Created_On,103) as Fecha
from Apuestas
order by try_convert(int,Apuesta) asc

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go
--grant exec on Wp_Get_Apuestas  to Prueba
go
Create table Ruleta_Apuestas(
	Id_Ruleta_Apuesta int identity(1,1) primary key ,
	Id_Ruleta int foreign key references Ruletas(Id_Ruleta),
	Ganador_Id_Apuesta int foreign key references Apuestas(Id_Apuesta),
	Created_On datetime default getdate()
)
go
Create table Apuestas_Usuarios(
Id_Apuesta_Usuario int identity(1,1) primary key ,
Usuario varchar(1000),
Id_Apuesta int foreign key references Apuestas(Id_Apuesta),
Valor_Apostado numeric(18,2),
Es_Ganador bit default(0),
Id_Ruleta_Apuesta int foreign key references Ruleta_Apuestas(Id_Ruleta_Apuesta),
)
go
create function [dbo].[Fn_List](@Text varchar(max), @Separator varchar(1))
returns @table table (Id int identity,Value varchar(max))
as begin
	declare @Pos1 int = 0,
			@Pos2 int = charindex(@Separator, @Text, 1)
	while (@Pos2 != 0) begin
		insert into @table (Value)
		select substring(@Text, @Pos1, @Pos2 - @Pos1)
				where substring(@Text, @Pos1, @Pos2 - @Pos1) != ''
		set @Pos1 = @Pos2 + 1
		set @Pos2 = charindex(@Separator, @Text, @Pos2 + 1)
	end
	return
end
GO
create function [dbo].[Fn_Split10](@Text varchar(max), @Separator varchar(1))
returns @table table(Var01 varchar(max), Var02 varchar(max), Var03 varchar(max), Var04 varchar(max), Var05 varchar(max), Var06 varchar(max),
	Var07 varchar(max), Var08 varchar(max), Var09 varchar(max), Var10 varchar(max))
as begin
	declare @var01 varchar(max), @var02 varchar(max), @var03 varchar(max), @var04 varchar(max), @var05 varchar(max), @var06 varchar(max),
		@var07 varchar(max), @var08 varchar(max), @var09 varchar(max), @var10 varchar(max)
	declare @Pos1 int = 0,
			@Pos2 int = charindex(@Separator, @Text, 1)
	while (@Pos2 != 0 and (@var01 is null or @var02 is null or @var03 is null or @var04 is null or @var05 is null or @var06 is null or @var07 is null or
			@var08 is null or @var09 is null or @var10 is null )) begin
		if @var01 is null
			set @var01 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var02 is null
			set @var02 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var03 is null
			set @var03 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var04 is null
			set @var04 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var05 is null
			set @var05 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var06 is null
			set @var06 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var07 is null
			set @var07 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var08 is null
			set @var08 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var09 is null
			set @var09 =  substring(@Text, @Pos1, @Pos2 - @Pos1)
		else if @var10 is null
			set @var10 =  substring(@Text, @Pos1, @Pos2 - @Pos1)

		set @Pos1 = @Pos2 + 1
		set @Pos2 = charindex(@Separator, @Text, @Pos2 + 1)
	end
	insert into @table
	select @var01, @var02, @var03, @var04, @var05, @var06, @var07, @var08, @var09, @var10
	return
end
GO
create function [dbo].[Fn_ConvNum] (@Valor money) 
	returns varchar(50)
as begin
	if @Valor is null
		return null
	declare @rev varchar(50),
		@ret varchar(50), @len int, @act int
	set @rev = reverse(convert(varchar,convert(numeric(18), @Valor)))
	set @ret = ''
	set @len = len(@rev)
	set @act = 1
	while @act <= @len begin
		if @act > 2 and @act % 3 = 1
			set @ret =  '.' + @ret
		set @ret =  substring(@rev,@act,1) + @ret 
		set @act = @act + 1
	end

	return '$ '+ @ret
end
GO



-- =============================================
/*
Author:	Santiago Reyes 
Create date: 06-10-2021 
Description: procedimiento encargado de determinar el ganador y guardar los registros
*/
-- =============================================

create Procedure  Wp_Ins_Apuestas_Usuarios
	@Id_Ruleta int,
	@apuestas varchar(max)
AS 
Begin
set transaction isolation level read uncommitted
Begin Try  

declare @ganador int = FLOOR(RAND()*(36-1+1))+1
declare @id_Ruleta_Apuestas int
declare @id_ganador_numero int
declare @id_ganador_color int

select @id_ganador_numero = Id_Apuesta
from Apuestas
where Apuesta = CONVERT(varchar,@ganador)

select @id_ganador_color = Id_Apuesta
from Apuestas
where Apuesta in (select case when Color = 'N'then 'Negro' else 'Rojo' end from Apuestas where Id_Apuesta = @id_ganador_numero)

/*proceso a ejecutar*/
insert into Ruleta_Apuestas(Id_Ruleta, Ganador_Id_Apuesta)
select @Id_Ruleta, @id_ganador_numero

set @id_Ruleta_Apuestas = @@IDENTITY

insert into Apuestas_Usuarios(Usuario,Id_Apuesta,Valor_Apostado,Es_Ganador,Id_Ruleta_Apuesta)
select 
	b.Var01,
	Convert(int,b.Var02),
	b.Var03,
	case when Convert(int,b.Var02) in (@id_ganador_numero, @id_ganador_color) then 1 else 0 end,
	@id_Ruleta_Apuestas
from [dbo].[Fn_List](@apuestas,'�') as a
cross apply [dbo].[Fn_Split10](a.Value,'|') as b

select 'El ganador es el Numero : '+CONVERT(varchar ,@ganador) +' </br></br> '+
replace(replace((
select 
	' El jugador : '+ a.Usuario + ' , '+
	CASE WHEN a.Es_Ganador = 1 
			THEN ' gano la suma de '+ [dbo].[Fn_ConvNum](a.Valor_Apostado * b.Premio)
		else ' perdio la suma de' + [dbo].[Fn_ConvNum](a.Valor_Apostado)
		end + ' </br> '
from Apuestas_Usuarios as a
left join Apuestas as b on a.Id_Apuesta = b.Id_Apuesta
where Id_Ruleta_Apuesta = @id_Ruleta_Apuestas
for xml path('')
),'&lt;','<'),'&gt;','>')

End Try          
Begin catch       
  SELECT 'Error - ' + error_message()           
End Catch  
End
go

