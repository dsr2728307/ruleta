﻿var GlobalVariables = {
    serviceObjectUrl: "./WMain/WServices/GenericObject.ashx",
    UDNBD: "prueba"
};
var indexMainDiv = document.getElementById("mainContentDiv");
var mainVue = new Vue({
    el: "#mainDiv",
    data: {
        /*1 = insertar, 2 = Actualizar, 3= listado*/
        proceso : 3,
        parsUsuario: {
            Id_Tecnico : 0,
            Nombre: "",
            Codigo : "",
            Sueldo_Base: "0",
            Id_Sucursal: 0,
            Elementos: []
        },
        elemento: {
            Cantidad: "1",
            Elemento: "0"
        },
        Elementos: [],
        sucursal : [],
        Usuarios: [],
        filtro:""
    },
    mounted: function(){
        
        var self = this;
        self.Datos_Iniciales()
        self.Get_Usuarios();
        document.getElementById("pDiv").style.display = "block";
    },
    methods: {
        InsTecnico: function () {
            var self = this;
            var errorMessageGuardar = "";

            if (self.parsUsuario.Nombre == "") {
                errorMessageGuardar = "Error - Debe llenar el campo Nombre";
            }
            else if (self.parsUsuario.Codigo == "") {
                errorMessageGuardar = "Error - Debe el campo codigo";
            }
            else if (self.parsUsuario.Sueldo_Base == "" || self.parsUsuario.Sueldo_Base == null) {
                errorMessageGuardar = "Error - el sueldo base es erroneo";
            }
            else if (self.parsUsuario.Id_Sucursal== "0" ) {
                errorMessageGuardar = "Error - Debe seleccionar una sucursal";
            }
            else if (self.parsUsuario.Elementos.length < 1) {
                errorMessageGuardar = "Error - debe agregar minimo un elemento";
            }
            if (errorMessageGuardar != "") {
                showMessage(errorMessageGuardar);
                return;
            }
            var elements = "";
            for (var i = 0; i < self.parsUsuario.Elementos.length; i++) {
                elements += self.parsUsuario.Elementos[i].Id_Elemento + ";" + self.parsUsuario.Elementos[i].Cantidad + ";|";
            }

            var par = {
                Id_Tecnico: self.parsUsuario.Id_Tecnico,
                Nombre: self.parsUsuario.Nombre,
                Codigo: self.parsUsuario.Codigo,
                Sueldo_Base: self.parsUsuario.Sueldo_Base,
                Id_Sucursal: self.parsUsuario.Id_Sucursal,
                elements: elements
            };
            showProgress();


            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_ST, "Wp_Ins_Tecnicos", GlobalVariables.UDNBD, par, function (data) {

                    if (!data.isError) {
                        self.reset_pars();
                        self.Get_Usuarios();
                    }
                    showMessage(data.data);

                    hideProgress();
                });
            
        },
        DelElemento: function (elemento) {
            var self = this;
            self.parsUsuario.Elementos = self.parsUsuario.Elementos.filter(item => item !== elemento)
        },
        InsElemento: function () {
            var self = this;
            var errorMessageGuardar = "";

            if (self.elemento.Cantidad == "" || self.elemento.Cantidad == null) {
                errorMessageGuardar = "Error - Cantidad Invalida";
            }
            else if (self.elemento.Cantidad < 0 || self.elemento.Cantidad > 10) {
                errorMessageGuardar = "Error - Cantidad  fuera de los limites";
            }
            else if (self.elemento.Elemento == "0") {
                errorMessageGuardar = "Error - Debe seleccionar un elemento";
            }
            if (self.parsUsuario.Elementos.find(item => item.Id_Elemento == self.elemento.Elemento.Id_Elemento)) {
                errorMessageGuardar = "Error - El Gasto ya esta registrado";
            }

            if (errorMessageGuardar != "") {
                showMessage(errorMessageGuardar);
                return;
            }
            

            self.parsUsuario.Elementos.push({ Cantidad: self.elemento.Cantidad, Id_Elemento: self.elemento.Elemento.Id_Elemento, Nombre: self.elemento.Elemento.Nombre })
            self.elemento = {
                Cantidad: "1",
                Elemento: "0"
            };
        },
        reset_pars: function () {
            var self = this;
            self.proceso = 3,
            self.parsUsuario = {
                Id_Tecnico: 0,
                Nombre: "",
                Codigo : "",
                Sueldo_Base: "0",
                Id_Sucursal: 0,
                Elementos: []
            }
        },
        Get_Usuarios: function () {
            var self = this;
            var pars = {
                filtro: self.filtro
            };
            showProgress();
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_DT, "Wp_Get_Tecnicos", GlobalVariables.UDNBD, pars, function (data) {

                if (!data.isError) {
                    self.Usuarios = data.data;
                } else {
                }
                hideProgress();
            });
        },

        Datos_Iniciales: function () {
            var self = this;
            showProgress();
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_DS, "WP_Get_Datos_Iniciales", GlobalVariables.UDNBD, null, function (data) {

                if (!data.isError) {
                    self.sucursal = data.data[0];
                    self.Elementos = data.data[1];
                } 
                hideProgress();
            });
        },

        UpdUsuario: function (Usuario) {
            var self = this;
            var pars = {
                Id_Tecnico: Usuario.Id_Tecnico
            };
            showProgress();
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_DS, "Wp_Get_Tecnico", GlobalVariables.UDNBD, pars, function (data) {

                if (!data.isError) {
                    self.proceso = 2;
                    self.parsUsuario.Id_Tecnico = data.data[0][0].Id_Tecnico;
                    self.parsUsuario.Nombre = data.data[0][0].Nombre;
                    self.parsUsuario.Codigo = data.data[0][0].Codigo;
                    self.parsUsuario.Sueldo_Base = parseFloat(data.data[0][0].Sueldo_Base);
                    self.parsUsuario.Id_Sucursal = data.data[0][0].Id_Sucursal;
                    self.parsUsuario.Elementos = data.data[1];
                } 
                hideProgress();
            });

        },
        
        DelUsuario: function (id) {
            var self = this;
            var par = {
                Id_Tecnico: id
            }

            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_ST, "WP_Del_Usuario", GlobalVariables.UDNBD, par, function (data) {

                if (!data.isError) {
                    self.reset_pars();
                    self.Get_Usuarios();
                    self.proceso = 3;
                }
                showMessage(data.data);
                hideProgress();
            });
        },
        
    }
});
/****************************************************************************************************************************************/
/******************************************************* COMMON FUNCTIONS ***************************************************************/
/****************************************************************************************************************************************/
function showProgress() {
    document.getElementById("divProcess").style.display = "block";
    return false;
}
function hideProgress() {
    document.getElementById("divProcess").style.display = "none";
    return false;
}
function showMessage(msg) {
    if (msg.indexOf("Error") == 0)
        document.getElementById("lbMessage").style.color = "red";
    else document.getElementById("lbMessage").style.color = "black";

    document.getElementById("lbMessage").innerText = msg;
    document.getElementById("divMessage").style.display = "block";
    document.getElementById("btAccept").focus();
    return false;
}
function hideMessage() {
    document.getElementById("divMessage").style.display = "none";
    hideProgress();
    return false;
}