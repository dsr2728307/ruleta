﻿using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web;

/// <summary>
/// Summary description for BDUt
/// </summary>
public static class BDUt
{
    public static string executeSp_ST(string comando, string[] nom_par, object[] val_par, string str_conn) {
        string ret = "";
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    conn.Open();
                    ret = cmd.ExecuteScalar().ToString();
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            ret = "Error - " + e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static string executeSp_ST(string comando, string nom_par, object val_par, string str_conn) {
        string ret = "";
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    cmd.Parameters.AddWithValue(nom_par, val_par);
                    conn.Open();
                    ret = cmd.ExecuteScalar().ToString();
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            ret = "Error - " + e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            text += "Pars: ";
            text += (nom_par ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            text += (val_par == null ? "null" : val_par.ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static DataTable executeSp_DT(string comando, string[] nom_par, object[] val_par, string str_conn) {
        DataTable ret = new DataTable();
        SqlConnection conn = null;
        SqlCommand cmd = null;
        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(ret);
                    conn.Close();
                    da.Dispose();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static DataSet executeSp_DS(string comando, string[] nom_par, object[] val_par, string str_conn) {
        DataSet ret = new DataSet();
        SqlConnection conn = null;
        SqlCommand cmd = null;

        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(ret);
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static DataSet executeSp_DS(string comando, string[] nom_par, object[] val_par, string str_conn, out string exception) {
        DataSet ret = new DataSet();
        SqlConnection conn = null;
        SqlCommand cmd = null;

        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(ret);
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
            exception = null;
        } catch (Exception e) {
            exception = e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public  static string setToJson(System.Data.DataSet ds) {
        System.Data.DataTable dt;
        JArray ret = new JArray(), mid;
        JObject obj;
        JValue token;
        for (int k = 0; k < ds.Tables.Count; k++) {
            mid = new JArray();
            dt = ds.Tables[k];
            for (int i = 0; i < dt.Rows.Count; i++) {
                obj = new JObject();
                for (int j = 0; j < dt.Columns.Count; j++) {
                    token = new JValue(dt.Rows[i][j].ToString());
                    obj.Add(dt.Columns[j].ColumnName, token);
                }
                mid.Add(obj);
            }
            ret.Add(mid);
        }
        return ret.ToString();
    }
    public  static string tableToJson(System.Data.DataTable dt) {
        JArray ret = new JArray();
        JObject obj;
        JValue token;
        for (int i = 0; i < dt.Rows.Count; i++) {
            obj = new JObject();
            for (int j = 0; j < dt.Columns.Count; j++) {
                token = new JValue(dt.Rows[i][j].ToString());
                obj.Add(dt.Columns[j].ColumnName, token);
            }
            ret.Add(obj);
        }
        return ret.ToString();
    }
    public static void log(string text) {
        try {
            if (HttpContext.Current != null)
                File.AppendAllText(HttpContext.Current.Server.MapPath(" ~/Data/log.txt"), DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "\t" + text + Environment.NewLine);
        } catch { }
    }
    /*
     * Estos nuevos procedimientos retornan un objecto JSON con el respectivo mensaje de error (de suceder). El Objeto tiene esta estructura:
     * {data: <DT|DS|ST>, isError: <true|false>, errorMessage: <string>}
     */
    public const string DATA = "data", ISERROR = "isError", ERRORMESSAGE = "errorMessage", TYPE = "Type", PARAMETER = "Parameter";
    public static JObject executeSp_OST(string comando, string[] nom_par, object[] val_par, string str_conn) {
        JObject ret = new JObject();
        ret[ISERROR] = false;
        SqlConnection conn;
        SqlCommand cmd;
        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    conn.Open();
                    ret[DATA] = cmd.ExecuteScalar().ToString();
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            ret[ISERROR] = true;
            ret[ERRORMESSAGE] = e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static JObject executeSp_ODT(string comando, string[] nom_par, object[] val_par, string str_conn) {
        JObject ret = new JObject();
        ret[ISERROR] = false;
        DataTable dataTable = new DataTable();
        SqlConnection conn;
        SqlCommand cmd;
        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(dataTable);
                    ret[DATA] = tableToJObject(dataTable);
                    conn.Close();
                    da.Dispose();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            ret[ISERROR] = true;
            ret[ERRORMESSAGE] = e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static JObject executeSp_ODS(string comando, string[] nom_par, object[] val_par, string str_conn) {
        JObject ret = new JObject();
        DataSet dataSet = new DataSet();
        SqlConnection conn;
        SqlCommand cmd;

        try {
            using (conn = new SqlConnection(str_conn)) {
                using (cmd = new SqlCommand(comando, conn)) {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 3600 * 2;
                    if (nom_par != null && val_par != null)
                        for (int i = 0; i < Math.Min(nom_par.Length, val_par.Length); i++)
                            cmd.Parameters.AddWithValue(nom_par[i], val_par[i]);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    conn.Open();
                    da.Fill(dataSet);
                    ret[DATA] = setToJObject(dataSet);
                    conn.Close();
                    cmd.Dispose();
                    conn.Dispose();
                }
            }
        } catch (Exception e) {
            ret[ISERROR] = true;
            ret[ERRORMESSAGE] = e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (comando ?? "") + Environment.NewLine;
            //text += "CS: " + (str_conn ?? "") + Environment.NewLine;
            text += "Pars: ";
            for (int i = 0; i < nom_par.Length; i++)
                text += (nom_par[i] ?? "null") + "|";
            text += Environment.NewLine;
            text += "Vals: ";
            for (int i = 0; i < val_par.Length; i++)
                text += (val_par[i] == null ? "null" : val_par[i].ToString()) + "|";
            log(text);
        }
        return ret;
    }
    public static JArray setToJObject(System.Data.DataSet ds) {
        System.Data.DataTable dt;
        JArray ret = new JArray(), mid;
        JObject obj;
        JValue token;
        for (int k = 0; k < ds.Tables.Count; k++) {
            mid = new JArray();
            dt = ds.Tables[k];
            for (int i = 0; i < dt.Rows.Count; i++) {
                obj = new JObject();
                for (int j = 0; j < dt.Columns.Count; j++) {
                    token = new JValue(dt.Rows[i][j].ToString());
                    obj.Add(dt.Columns[j].ColumnName, token);
                }
                mid.Add(obj);
            }
            ret.Add(mid);
        }
        return ret;
    }
    public static JObject getProcedureParameters(string procedure, string str_conn)
    {
        JObject ret = new JObject();
        ret[ISERROR] = false;
        try
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add(PARAMETER);
            dataTable.Columns.Add(TYPE);
            using (SqlConnection conn = new SqlConnection(str_conn))
            {
                using (SqlCommand cmd = new SqlCommand(procedure, conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    conn.Open();
                    SqlCommandBuilder.DeriveParameters(cmd);
                    foreach (SqlParameter p in cmd.Parameters)
                        dataTable.Rows.Add(p.ParameterName, p.SqlDbType.ToString());
                }
            }
            ret[DATA] = tableToJObject(dataTable);
        }
        catch (Exception e)
        {
            ret[ISERROR] = true;
            ret[ERRORMESSAGE] = e.Message;
            string text = e.Message + Environment.NewLine;
            text += "SP: " + (procedure ?? "") + Environment.NewLine;
            log(text);
        }
        return ret;
    }
    public static JArray tableToJObject(System.Data.DataTable dt) {
        JArray ret = new JArray();
        JObject obj;
        JValue token;
        for (int i = 0; i < dt.Rows.Count; i++) {
            obj = new JObject();
            for (int j = 0; j < dt.Columns.Count; j++) {
                token = new JValue(dt.Rows[i][j].ToString());
                obj.Add(dt.Columns[j].ColumnName, token);
            }
            ret.Add(obj);
        }
        return ret;
    }
}