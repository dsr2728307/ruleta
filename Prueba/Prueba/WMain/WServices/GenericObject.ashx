﻿<%@ WebHandler Language="C#" Class="Generic" %>

using System.Web.Security;
using System.Web;
using System.Web.Caching;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Configuration;
using System;
using System.Data;

public class Generic : IHttpHandler {
    public void ProcessRequest(HttpContext context) {
        context.Response.ContentType = "text/plain";
        string jsonData = "", op = context.Request["op"];
        using (StreamReader bodyStream = new StreamReader(HttpContext.Current.Request.InputStream)) {
            bodyStream.BaseStream.Seek(0, SeekOrigin.Begin);
            jsonData = bodyStream.ReadToEnd();
        }
        try {
            JObject data = null;
            if (jsonData != "")
                data = JObject.Parse(jsonData);
            if (op != null && (op.StartsWith("generic") ) && data != null)
            {
                string sp = data["sp"].Value<string>();
                string dbName = data["db"].Value<string>();
                string connectionString = GetConnectionStringByName(dbName, context);
                if (connectionString != null)
                {
                    JArray jpars = (JArray)data["pars"];
                    string[] pars = new string[jpars.Count];
                    object[] vals = new object[jpars.Count];
                    string par, lcapr, val;
                    for (int i = 0; i < jpars.Count; i++)
                    {
                        par = jpars[i]["name"].Value<string>();
                        val = jpars[i]["value"].Value<string>();
                        lcapr = par.ToLower();
                        pars[i] = (par[0] == '@' ? "" : "@") + par;
                        vals[i] = val;
                    }

                    if (op.EndsWith("DS"))
                        context.Response.Write(BDUt.executeSp_ODS(sp, pars, vals, connectionString));
                    else if (op.EndsWith("DT"))
                        context.Response.Write(BDUt.executeSp_ODT(sp, pars, vals, connectionString));
                    else if (op.EndsWith("ST"))
                        context.Response.Write(BDUt.executeSp_OST(sp, pars, vals, connectionString));
                }
                else
                {
                    JObject ret = new JObject();
                    ret[BDUt.ERRORMESSAGE] = "Error - no DB";
                    ret[BDUt.ISERROR] = true;
                    context.Response.Write(ret);
                }
            }
            
        } catch (System.Exception ex) {
            JObject ret = new JObject();
            ret[BDUt.ERRORMESSAGE] = ex.Message;
            ret[BDUt.ISERROR] = true;
            context.Response.Write(ret);
            BDUt.log("Generic-ProcessRequest " + ex.Message + Environment.NewLine + jsonData);
        }
        context.Response.Flush();
        context.Response.End();
    }
    public string GetConnectionStringByName(string name, HttpContext context) {
        string cname = "CSC_" + name;
        if (context.Cache[cname] == null) {
            context.Cache.Insert(cname, ConfigurationManager.ConnectionStrings[name].ConnectionString, null, DateTime.Now.AddSeconds(600), Cache.NoSlidingExpiration);
        }
        object ret = context.Cache[cname];
        return ret == null? null: ret.ToString();
    }
    public bool IsReusable {
        get {
            return false;
        }
    }

}