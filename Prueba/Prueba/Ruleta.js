﻿var GlobalVariables = {
    serviceObjectUrl: "./WMain/WServices/GenericObject.ashx",
    UDNBD: "prueba"
};
var indexMainDiv = document.getElementById("mainContentDiv");
var mainVue = new Vue({
    el: "#mainDiv",
    data: {
        /*1 = Listado*/
        proceso: 1,
        listado: [],
        ruletaabierta: 0,
        apuesta: {
            Nombre: "",
            Apuesta: 0,
            ValorApuesta :0
        },
        apuestas: [],
        apuestas_usuarios: [],
    },
    mounted: function () {

        var self = this;
        self.Get_Ruletas();
        document.getElementById("pDiv").style.display = "block";
    },
    methods: {
        Ins_Ruleta: function () {
            var self = this;
            showProgress();
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_ST, "Wp_Ins_Ruleta", GlobalVariables.UDNBD, null, function (data) {
                self.Get_Ruletas();
                showMessage(data.data);
                hideProgress();
            });
        },
        Upd_Ruleta: function (item) {
            var self = this;
            showProgress();
            var pars = {
                Id_Ruleta: item.Id_Ruleta
            };
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_ST, "Wp_Upd_Ruletas", GlobalVariables.UDNBD, pars, function (data) {
                self.Get_Ruletas();
                showMessage(data.data);
                hideProgress();
            });
        },
        Get_Ruletas: function () {
            var self = this;
            showProgress();
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_DT, "Wp_Get_Ruletas", GlobalVariables.UDNBD, null, function (data) {
                self.listado = data.data;
                hideProgress();
            });
        },
        Ini_Apuestas: function (item) {
            var self = this;
            if (item.Estado == 'False') {
                showMessage("Error - No se puede abrir las apuestas de una ruleta cerrada");
                return;
            }
            self.proceso = 2;
            self.ruletaabierta = item.Id_Ruleta;
            self.Get_Apuestas();
            self.apuestas_usuarios = [];
        },
        Get_Apuestas: function () {
            var self = this;
            self.apuestas = {};
            showProgress();
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_DT, "Wp_Get_Apuestas", GlobalVariables.UDNBD, null, function (data) {
                self.apuestas = data.data;
                hideProgress();
            });
        },
        Ins_Apuestas_Usuario: function () {
            var self = this;

            if (self.apuesta.Nombre == "") {
                showMessage("Error - Debe escribir un nombre para quien apuesta");
                return;
            } else if (self.apuesta.Apuesta == 0) {
                showMessage("Error - Debe seleccionar una apuesta");
                return;
            }
            else if (self.apuesta.ValorApuesta == "" || self.apuesta.ValorApuesta == null) {
                showMessage("Error - Valor de la apuesta invalido");
                return;
            }
            else if (parseInt(self.apuesta.ValorApuesta) < 1 || parseInt(self.apuesta.ValorApuesta) > 10000) {
                showMessage("Error - Valor de la apuesta invalido");
                return;
            }
            var apuesta_nombre = "";
            self.apuestas.forEach(element => {
                if (element.Id_Apuesta == self.apuesta.Apuesta) apuesta_nombre = element.Apuesta;
                
            });

            self.apuestas_usuarios.push({ Nombre: self.apuesta.Nombre, Apuesta: self.apuesta.Apuesta, ValorApuesta: self.apuesta.ValorApuesta, apuesta_nombre: apuesta_nombre });



            self.apuesta = {
                Nombre: "",
                Apuesta: 0,
                ValorApuesta: 0
            };
        },
        Cerrar_Apuestas: function () {
            var self = this;
            showProgress();
            var apuestas_usuarios_texto = "";
            self.apuestas_usuarios.forEach(element => {
                apuestas_usuarios_texto += element.Nombre + "|" + element.Apuesta + "|" + element.ValorApuesta + "|¬";

            });

            var pars = {
                Id_Ruleta: self.ruletaabierta,
                apuestas: apuestas_usuarios_texto
            };
            httpCallSpO(GlobalVariables.serviceObjectUrl, AxiosConst.GENERIC_ST, "Wp_Ins_Apuestas_Usuarios", GlobalVariables.UDNBD, pars, function (data) {
                self.apuestas_usuarios = [];
                showMessage(data.data);
                hideProgress();
            });
        },
    }
});
/****************************************************************************************************************************************/
/******************************************************* COMMON FUNCTIONS ***************************************************************/
/****************************************************************************************************************************************/
function showProgress() {
    document.getElementById("divProcess").style.display = "block";
    return false;
}
function hideProgress() {
    document.getElementById("divProcess").style.display = "none";
    return false;
}
function showMessage(msg) {
    if (msg.indexOf("Error") == 0)
        document.getElementById("lbMessage").style.color = "red";
    else document.getElementById("lbMessage").style.color = "black";

    document.getElementById("lbMessage").innerHTML = msg;
    document.getElementById("divMessage").style.display = "block";
    document.getElementById("btAccept").focus();
    return false;
}
function hideMessage() {
    document.getElementById("divMessage").style.display = "none";
    hideProgress();
    return false;
}