﻿var AxiosConfig = {
    ver: "1"
};
var AxiosMethods = {
    loginMethod: null
};
var AxiosConst = {
    GENERIC_DT: "genericDT",
    GENERIC_DS: "genericDS",
    GENERIC_ST: "genericST"
}
function httpCallSp(url, callType,sp, db, parsObject, success, error) {
    var postData = { pars: [], sp: sp, db: db };
    for (var name in parsObject) 
        postData.pars.push({ name: name, value: parsObject[name] + "" });
    if (callType == AxiosConst.GENERIC_DS || callType == AxiosConst.GENERIC_DT || callType == AxiosConst.EXPORT_DATA)
        httpCallObj(url + "?op=" + callType, postData, success, error);
    else 
        httpCallStr(url + "?op=" + callType, postData, success, error);
}
function httpCallSpO(url, callType, sp, db, parsObject, success, error) {
    var postData = { pars: [], sp: sp, db: db };
    for (var name in parsObject)
        postData.pars.push({ name: name, value: parsObject[name] + "" });
    httpCallObj(url + "?op=" + callType, postData, success, error);
}
function httpCallObj(url, data, success, error) {
    url = getUrlParameters(url);
    axios.post(url, data)
        .then(function (response) {
            try {
                var obj;
                if (typeof response.data === 'string' || response.data instanceof String){
                    if(response.data.indexOf("<html")>= 0){
                        if(AxiosMethods.loginMethod != null)
                            AxiosMethods.loginMethod();
                        return;
                    } else obj = JSON.parse(response.data);
                } else obj = response.data;
                success(obj);
            } catch (e) {
                console.log(url);
                console.log(e);
                console.log(response);
                if (error != null)
                    error(e);
            }
        })
        .catch(function (err) {
            console.log(url);
            console.log(err);
            if (error != null)
                error(err);
        });
}
function httpCallStr(url, data, success, error) {
    url = getUrlParameters(url);
    axios.post(url, data)
        .then(function (response) {
            success(response.data);
        })
        .catch(function (e) {
            console.log(e);
            if (error != null)
                error(e);
        });
}
function httpGetUrl(url, callback) {
    axios.get(url)
        .then(function (response) {
            callback(response.data);
        })
        .catch(function (error) {
            console.log(error);
            callback("");
        })
}
function getUrlParameters(url) {
    var ret = "";
    for (var key in AxiosConfig) {
        if (ret != "") ret += "&";
        ret += key + "=" + AxiosConfig[key];
    }
    if (url.indexOf("?") >= 0) ret = url + "&" + ret;
    else ret += url + "?" + ret;
    return ret;
}